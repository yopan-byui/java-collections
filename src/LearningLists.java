import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LearningLists {
    public static void main(String[] args) {

        // A generic list, with no types
        List firstList = new ArrayList();

        // A typed List
        List<String> secondList = new ArrayList<String>();

        // The add method adds to the end of the List
        firstList.add("Begin");
        firstList.add("Hello");
        firstList.add("My");
        firstList.add("Friend");
        firstList.add("End");

        // The remove method needs the index of the item that we want to remove
        firstList.remove(0);

        // The get method will access an item of a specific index position
        // Then we can store this item into its own variable
        String anObject = (String) firstList.get(0);
        System.out.println("## Print first item, after removal");
        System.out.println(anObject);

        // The size method returns an integer with the number of items within a List

        int sizeOfTheList = firstList.size();
        System.out.println("## Print list size");
        System.out.println(sizeOfTheList);


        // ##############################
        // ### WAYS TO ITERATE A LIST ###
        // ##############################

        // #01 - Iterator
        Iterator iterator = firstList.iterator();
        // hasNext will return true until there is items in the List
        System.out.println("## Print all items with iterator");
        while(iterator.hasNext()) {
            Object next = iterator.next();
            System.out.println(next);
        }

        // 02 - Next Loop
        System.out.println("## Print all items with a next loop");
        for(Object item : firstList) {
            System.out.println(item);
        }

        // The clear method empties the List
        firstList.clear();
        firstList.add("Lone item");

        // 03 - Traditional For Loop
        // This one is not very good for LinkedLists
        System.out.println("## Print lonely item with a for loop");
        for(int i = 0; i < firstList.size(); i++) {
            System.out.println(firstList.get(i));
        }
    }
}

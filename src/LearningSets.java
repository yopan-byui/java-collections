import java.util.*;

public class LearningSets {
    public static void main(String[] args) {

        // Unordered set
        Set<String> hashSet = new HashSet<String>();

        hashSet.add("Hello");
        hashSet.add("1");
        boolean added01 = hashSet.add("My");
        // It's not possible to repeat and entry.
        // Sets will only accept unique entries.
        boolean added02 = hashSet.add("My");

        System.out.println("## Test repeated insertion");
        System.out.println(added01);
        System.out.println(added02);

        // Creating a set and passing values to it right in its creation
        Set <String> set02 = Set.of("Hello", "1", "My", "2", "Friend");

        // Iterating through a hash set using Iterator
        // The order of a set insertion won't be respected
        // A Hash set has no order whatsoever
        System.out.println("## See unordered Set");
        Iterator<String> iterator = set02.iterator();
        while(iterator.hasNext()) {
            String next = iterator.next();
            System.out.println(next);
        }

        // Ordered set
        Set<String> treeSet = new TreeSet();
        treeSet.add("hello");
        treeSet.add("1");
        treeSet.add("my");
        treeSet.add("!");
        treeSet.add("?");
        treeSet.add("2");
        treeSet.add("friend");
        treeSet.add("3");
        treeSet.add("Begin");

        // We can remove an element from a set:
        treeSet.remove("?");

        // Iterating through a tree set using a Loop
        // The Tree Set will order symbols > numbers > Capital letters (alphabetically) > small caps (alphabetically)...
        System.out.println("## See ordered Set");
        for(String element : treeSet) {
            System.out.println(element);
        }

        // We can also add all elements from a collection to a Set
        hashSet.addAll(Set.of("9", "1", "2", "Friend", "3"));
        // There is an equivalent removeAll method
        System.out.println("## Print original hashSet with extra elements");
        System.out.println(hashSet);

        // There is a very interesting method called retain
        // It will only keep the items that were in the original Set
        // and that are being listed in the current collection
        Set<String> finalSet = new HashSet<String>();
        finalSet.addAll(Set.of("A", "B", "C", "D", "E"));
        finalSet.removeAll( Set.of("B", "D"));
        finalSet.retainAll( Set.of("A", "B", "C", "D", "E", "F", "G"));
        System.out.println("## Print the retained items");
        System.out.println(finalSet);
        System.out.println("## Notice that the B and D that were removed, and the F and G that weren't originally, are not in the final set.");

        // We can check if a Set contain a specific element
        boolean containsA = finalSet.contains("A");
        boolean containsB = finalSet.contains("B");
        boolean containsG = finalSet.contains("G");

        System.out.println("## Print the booleans for contain verifications");
        System.out.println(containsA);
        System.out.println(containsB);
        System.out.println(containsG);

        // We can also convert a Set to a List
        // We do that by simply passing the Set to a List
        List<String> finalList = new ArrayList<String>();
        finalList.addAll(finalSet);

        System.out.println("## Print the set converted into a List");
        System.out.println(finalList);
    }
}

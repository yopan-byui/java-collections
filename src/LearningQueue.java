import java.util.*;

public class LearningQueue {
    public static void main(String[] args) {
        // Queues' process is also known as First In First out or FiFo
        // Queue is an interface and not a Class itself
        // It's good practice to specify a generic type for a Queue
        Queue<String> queue = new LinkedList<String>();

        queue.add("Larissa");
        queue.add("Arthur");

        String larissa = queue.poll();
        String arthur = queue.poll();

        System.out.println(larissa);
        System.out.println(arthur);

        // There are 3 classes that can be used as Queues:
        // LinkedList, ArrayDeque & PriorityQueue
        // The most used is the LinkedList

        // We can add an item to a queue either using
        // ADD or OFFER methods.
        // Add throws an exception if the queue is full
        // Offer returns false if the queue is full

        queue.add("Julia");
        queue.offer("Sofia");
        System.out.println("## Print new items of the queue");
        System.out.println(queue);

        // There are also two methods to take elements out
        // REMOVE or POLL methods.
        // Remove throws an exception if there are no elements to take
        // Poll returns null if there are no elements to take

        queue.remove();
        queue.poll();
        System.out.println("## Print left items of the queue");
        System.out.println(queue);

        // We can look at the first element of a queue without removing it
        queue.offer("Kiara");
        String firstItem = queue.peek();
        System.out.println("## Print a peek from the queue");
        System.out.println(firstItem);

        // Beside Iterator and Loop to iterate through each element of a collection
        // we can also use the Java Streams API

        queue.offer("Simba");
        queue.offer("Nala");
        queue.offer("Scar");
        System.out.println("## Print each item in the queue");
        queue.stream().forEach((element) -> {
            System.out.println(element);
        });

    }
}


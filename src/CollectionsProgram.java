import java.util.*;

public class CollectionsProgram {
    public static void main(String[] args) {
        Hashtable<Integer, String> options = createOptions();

        List<String> playerWinningHands = new ArrayList<String>();
        Set<String> computerWinningHands = new HashSet<String>();
        Integer draws = 0;

        Set<String> playersHands = new TreeSet();

        Scanner scanner = new Scanner(System.in);

        String currentPlayer = "";
        currentPlayer = welcomingText(scanner);

        welcomeUser(currentPlayer);

        boolean gameStarted = false;

        if(currentPlayer != "") {
            gameStarted = true;
        }

        while(gameStarted) {
            currentScores(playerWinningHands, computerWinningHands, draws);
            playersLastHands(playersHands);

            Integer currentPlay = usersCurrentPlay(scanner);
            Integer computersPlay = computersCurrentPlay();

            if(currentPlay != 0) {
                playersHands.add(options.get(currentPlay));
            }

            whoPlayedWhat(options, currentPlay, computersPlay);

            if(currentPlay == 0) {
                System.out.println("Thanks for playing");
                gameStarted = false;
            } else if(currentPlay == computersPlay) {
                draws += 1;
                System.out.println("It was a draw, try again!");
            } else if((currentPlay == 1 && computersPlay == 3) || (currentPlay == 2 && computersPlay == 1) || (currentPlay == 3 && computersPlay == 2)) {
                playerWinningHands.add(options.get(currentPlay));
                System.out.println("You won this one!");
            } else {
                computerWinningHands.add(options.get(computersPlay));
                System.out.println("You lost this one!");
            }
        }
    }

    public static Hashtable<Integer, String> createOptions(){
        Hashtable<Integer, String> options = new Hashtable<Integer, String>();
        options.put(1, "Rock");
        options.put(2, "Paper");
        options.put(3, "Scissors");
        return options;
    }

    public static String welcomingText(Scanner scanner) {
        System.out.println("##########################################");
        System.out.println("## Let's play Rock, Paper, Scissors?    ##");
        System.out.println("## Please ENTER player's name:          ##");
        System.out.println("##########################################");

        String currentPlayer = scanner.nextLine();
        return currentPlayer;
    }

    public static void currentScores(List<String> playerWinningHands, Set<String> computerWinningHands, Integer draws) {
        System.out.println("##########################################");
        System.out.println("## You: " + playerWinningHands.size() + "     Draws: " + draws + "    Computer: " + computerWinningHands.size() + "   ##");
        System.out.println("##########################################");
    }

    public static void playersLastHands(Set<String> playersHands) {
        if(playersHands.size() > 0) {
            System.out.print("Your recent hands: ");
        }

        for(String element : playersHands) {
            System.out.print(element);
            System.out.print(", ");
        }
    }

    public static void welcomeUser(String currentPlayer) {
        System.out.println("Welcome, " + currentPlayer + "! Pick your alternative.");
    }

    public static Integer computersCurrentPlay() {
        Queue<Integer> computersFuturePlays = new LinkedList<>();

        Random random = new Random();
        computersFuturePlays.offer(random.nextInt(3 - 1) + 1);
        computersFuturePlays.offer(random.nextInt(3 - 1) + 1);
        computersFuturePlays.offer(random.nextInt(3 - 1) + 1);

        return computersFuturePlays.poll();
    }

    public static Integer usersCurrentPlay(Scanner scanner) {
        System.out.println(" ");
        System.out.println("1 - Rock | 2 - Paper | 3 - Scissors | 0 - Exit");
        System.out.println("Your play: ");
        Integer currentPlay = Integer.parseInt(scanner.nextLine());
        return currentPlay;
    }

    public static void whoPlayedWhat(Hashtable<Integer, String> options, Integer currentPlay, Integer computersPlay) {
        System.out.println("You played '" + options.get(currentPlay) + "' and the Computer played '" + options.get(computersPlay) + "'!");
    }
}
